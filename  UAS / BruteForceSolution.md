# Soal No 3 
## Penjelasan Penyelesaian

|Langkah|Keterangan|
|--| -------- |
|1.|Tentukan titik-titik perhentian yang tersedia. Dalam kasus ini, titik-titik warung yang tersedia adalah 10, 25, 30, 40, 50, 75, 80, 110, dan 130 km dari titik awal| 
|2.|Mulai dari jarak awal (0 km), iterasi melalui setiap titik warung yang tersedia|
|3.|Saat iterasi, cek apakah jarak warung tersebut dengan jarak terakhir kurang dari atau sama dengan 30 km. Jika iya, tambahkan warung tersebut sebagai perhentian optimal dan perbaharui jarak terakhir|
|5.|Ulangi langkah 3 hingga mencapai jarak akhir (140 km)| 
|6.|Tampilkan titik-titik perhentian optimal sebagai output|

## Kode Program Dalam Bahasa Python
```python
def find_optimal_stops(stops, target_distance, rest_distance):
    optimal_stops = []
    last_stop = 0
    
    for i in range(len(stops)):
        if stops[i] - last_stop > rest_distance:
            optimal_stops.append(stops[i-1])
            last_stop = stops[i-1]
    
    optimal_stops.append(stops[-1])
    return optimal_stops


# Daftar titik-titik warung yang tersedia
stops = [10, 25, 30, 40, 50, 75, 80, 110, 130]

# Jarak total yang ingin ditempuh
target_distance = 140

# Jarak antara setiap perhentian untuk minum
rest_distance = 30

# Cari titik-titik perhentian optimal
optimal_stops = find_optimal_stops(stops, target_distance, rest_distance)

# Tampilkan hasil
print("Titik-titik perhentian optimal:")
for stop in optimal_stops:
    print(stop, "km")
```
## Output
### GIF
![gifKode](https://gitlab.com/Mokyra18/strategialgoritma/-/raw/main/%20UAS/SS/Output.gif)

### SS
![SSKode](https://gitlab.com/Mokyra18/strategialgoritma/-/raw/main/%20UAS/SS/Output.png)
